<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;
use Validator;
// model
use App\Model\Asuransi;
use App\Model\User;
use App\Model\UserToken;
use App\Model\UserProfile;

//use DB;

class LoginController extends Controller {

    function login(Request $request) {
        // validate field
        $validator = Validator::make($request->all(), array(
                    'email' => 'required|email',
                    'password' => 'required',
                    'device_id' => 'required',
        ));

        if ($validator->fails()) {
            return response()->json([
                        'code' => 200,
                        'status' => '0',
                        'message' => 'field required',
                        'data' => $validator->messages()
                            ], 200);
        }

        $user = User::where('email', $request->input('email'))->first();
        if ($user && $user->status == '1' && Hash::check($request->input('password'), $user->password)) {
            $token = new UserToken;
            $token->user_id = $user->id;
            $token->token = Hash::make($user->id . uniqid(15));
            $token->device_id = $request->input('device_id');
            $token->save();

            // get user profile
            $user_data = UserProfile::where('user_id', $user->id)->first();
            $user_data->token = $token->token;
            $user_data->device_id = $token->device_id;

            return response()->json([
                        'code' => 200,
                        'status' => '1',
                        'message' => 'Success Login',
                        'data' => $user_data,
                            ], 200);
        }

        return response()->json([
                    'code' => 200,
                    'status' => '1',
                    'message' => 'Wrong password or email',
                    'data' => '',
                        ], 200);
    }

    function logout(Request $request) {
        // validate field
        $validator = Validator::make($request->all(), array(
                    'token' => 'required',
                    'device_id' => 'required',
        ));

        if ($validator->fails()) {
            return response()->json([
                        'code' => 200,
                        'status' => '0',
                        'message' => 'field required',
                        'data' => $validator->messages()
                            ], 200);
        }

        $delete = UserToken::where('token', $request->input('token'))
                ->where('device_id', $request->input('device_id'))
                ->delete();

        return response()->json([
                    'code' => 200,
                    'status' => '1',
                    'message' => 'Success Logout',
                    'data' => '',
                        ], 200);
    }
    
    function test(Request $request){
        return 'true';
    }

}

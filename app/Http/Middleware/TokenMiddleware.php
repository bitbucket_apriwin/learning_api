<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\UserToken;

class TokenMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
//    protected $token;

    public function handle($request, Closure $next) {
//        return $request->header('device-id');
        // check user token for mobile
        $user_id = $this->checkToken($request->header('token'), $request->header('device-id'));
        if (!$user_id) {
            return response()->json([
                        'code' => 200,
                        'type' => 'failed',
                        'status' => 'EXPIRED_TOKEN',
                        'message' => 'expired_token'
                            ], 200);
        }

        //return user id value to controller
        $request->user_id = $user_id;
        return $next($request);
    }

    public function checkToken($token, $device_id) {
        //check token
        $user_token = UserToken::select('user_id')
                ->where('token', $token)
                ->where('device_id', $device_id)
                ->first();
        // return user id if token not expired
        if ($user_token) {
            return $user_token->user_id;
        }
        return false;
    }

}

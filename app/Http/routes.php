<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('test', 'TestController@index');

// before login api
$app->post('login', 'LoginController@login');

$app->post('logout', 'LoginController@logout');

// api with middleware token
$app->group(['middleware' => 'token', 'prefix' => 'user', 'namespace' => 'App\Http\Controllers'], function () use ($app) {
    $app->post('testmiddleware', 'LoginController@test');
});
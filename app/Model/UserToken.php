<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model {

    protected $table = "user_token";
    protected $hidden = [
        'id', 'user_id',
    ];

}
